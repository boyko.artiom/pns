use bytes::BufMut;
use md5::Digest;
use std::borrow::Cow;
use std::ops::Deref;
use tokio::io::{AsyncReadExt, AsyncWriteExt};

const SEGMENT_BITS: u8 = 0x7F;
const CONTINUE_BIT: u8 = 0x80;

async fn read_varint(
    mut socket: Option<&mut tokio::net::TcpStream>,
    buf: &mut bytes::BytesMut,
) -> Result<i32, anyhow::Error> {
    let mut value: i32 = 0;
    let mut position = 0;

    let mut bytes_read = 0;

    loop {
        let byte = if bytes_read < buf.len() {
            let byte = buf[bytes_read];
            bytes_read += 1;
            byte
        } else {
            buf.clear();
            socket
                .as_mut()
                .ok_or(anyhow::anyhow!("eof"))?
                .read_buf(buf)
                .await?;

            if buf.is_empty() {
                anyhow::bail!("eof");
            }

            bytes_read = 1;

            buf[0]
        };

        value |= i32::from(byte & SEGMENT_BITS) << position;

        if byte & CONTINUE_BIT == 0 {
            break;
        }

        position += 7;
        if position >= 32 {
            anyhow::bail!("varint is too long");
        }
    }

    let _ = buf.split_to(bytes_read);

    Ok(value)
}

async fn read_packet_bytes(
    socket: &mut tokio::net::TcpStream,
    buf: &mut bytes::BytesMut,
) -> Result<bytes::BytesMut, anyhow::Error> {
    let len = read_varint(Some(socket), buf).await?;

    if len < 1 {
        anyhow::bail!("invalid packet length");
    }

    let len = len as usize;

    while buf.len() < len {
        socket.read_buf(buf).await?;
    }

    Ok(buf.split_to(len))
}

#[derive(Debug)]
struct RawPacket {
    packet_id: i32,
    data: bytes::BytesMut,
}

async fn read_packet(
    socket: &mut tokio::net::TcpStream,
    buf: &mut bytes::BytesMut,
    compression_enabled: bool,
) -> Result<RawPacket, anyhow::Error> {
    let mut content = read_packet_bytes(socket, buf).await?;
    if compression_enabled {
        let data_len = read_varint(None, &mut content).await?;
        println!("data_len={}", data_len);
        if data_len != 0 {
            anyhow::bail!("compression is not implemented");
        }
    }

    let packet_id = read_varint(None, &mut content).await?;

    Ok(RawPacket {
        packet_id,
        data: content,
    })
}

async fn read_string(buf: &mut bytes::BytesMut, max_len: usize) -> Result<String, anyhow::Error> {
    let len = read_varint(None, buf).await?;
    if len < 0 {
        anyhow::bail!("invalid length");
    }

    let len = len as usize;

    if len > max_len * 4 {
        // max size of each character is 4 bytes, so in this case it's definitely too long
        anyhow::bail!("string is longer than max_len");
    }
    if len > buf.len() {
        anyhow::bail!("eof");
    }
    let src = buf.split_to(len);

    let result = String::from_utf8(src.to_vec())?;
    if result.len() > max_len {
        anyhow::bail!("string is longer than max_len");
    }

    Ok(result)
}

fn read_ushort(buf: &mut bytes::BytesMut) -> Result<u16, anyhow::Error> {
    if buf.len() < 2 {
        anyhow::bail!("eof");
    }
    let src = buf.split_to(2);
    Ok(u16::from_be_bytes([src[0], src[1]]))
}

fn write_varint(buf: &mut bytes::BytesMut, value: i32) {
    let mut value = u32::from_be_bytes(value.to_be_bytes());
    loop {
        if value & !(SEGMENT_BITS as u32) == 0 {
            buf.put_u8(value as u8);
            return;
        }

        buf.put_u8(((value as u8) & SEGMENT_BITS) | CONTINUE_BIT);

        value >>= 7;
    }
}

fn write_string(buf: &mut bytes::BytesMut, value: &str) {
    let len = value.len();
    write_varint(buf, len as i32);
    buf.extend_from_slice(value.as_bytes());
}

fn write_ushort(buf: &mut bytes::BytesMut, value: u16) {
    buf.put_u16(value);
}

fn write_uuid(buf: &mut bytes::BytesMut, value: uuid::Uuid) {
    buf.extend_from_slice(&value.as_bytes()[..]);
}

fn write_int(buf: &mut bytes::BytesMut, value: i32) {
    buf.put_i32(value);
}

fn write_bool(buf: &mut bytes::BytesMut, value: bool) {
    buf.put_u8(if value { 1 } else { 0 });
}

fn write_ubyte(buf: &mut bytes::BytesMut, value: u8) {
    buf.put_u8(value);
}

fn write_byte(buf: &mut bytes::BytesMut, value: i8) {
    buf.put_i8(value);
}

fn write_long(buf: &mut bytes::BytesMut, value: i64) {
    buf.put_i64(value);
}

fn write_double(buf: &mut bytes::BytesMut, value: f64) {
    buf.put_f64(value);
}

fn write_float(buf: &mut bytes::BytesMut, value: f32) {
    buf.put_f32(value);
}

fn write_position(buf: &mut bytes::BytesMut, x: i64, y: i64, z: i64) {
    let value = ((x & 0x3FFFFFF) << 38) | ((z & 0x3FFFFFF) << 12) | (y & 0xFFF);
    buf.put_i64(value);
}

async fn send_packet(
    socket: &mut tokio::net::TcpStream,
    packet_id: i8,
    content: &[u8],
    compression_enabled: bool,
) -> Result<(), anyhow::Error> {
    assert!(packet_id >= 0);

    println!("sending packet {} {:?}", packet_id, content);

    let mut len_buf = bytes::BytesMut::new();
    write_varint(
        &mut len_buf,
        (content.len() + if compression_enabled { 2 } else { 1 }) as i32,
    );
    if compression_enabled {
        len_buf.put_u8(0);
    }
    len_buf.put_u8(packet_id as u8);

    socket.write_all(&len_buf).await?;
    socket.write_all(&content).await?;

    Ok(())
}

async fn establish_upstream_connection() -> Result<tokio::net::TcpStream, anyhow::Error> {
    Ok(tokio::net::TcpStream::connect(
        match std::env::var("PNS_UPSTREAM_HOST") {
            Ok(value) => Cow::Owned(value),
            Err(std::env::VarError::NotPresent) => Cow::Borrowed("127.0.0.1:25565"),
            Err(err) => return Err(err.into()),
        }
        .deref(),
    )
    .await?)
}

enum ProtocolVersion {
    V1_19_1,
}

async fn handle_connection(
    mut socket: tokio::net::TcpStream,
    socket_addr: std::net::SocketAddr,
    db: sled::Db,
) -> Result<(), anyhow::Error> {
    let mut buf = bytes::BytesMut::new();
    let first_packet = read_packet(&mut socket, &mut buf, false).await?;

    println!("packet = {:?}", first_packet);

    if first_packet.packet_id != 0 {
        anyhow::bail!("unexpected packet during handshake");
    }

    let mut pbuf = first_packet.data;

    let version_code = read_varint(None, &mut pbuf).await?;
    let _version = match version_code {
        760 => ProtocolVersion::V1_19_1,
        _ => {
            let mut kick_packet_buf = bytes::BytesMut::new();
            write_string(&mut kick_packet_buf, r#"{"text": "Malĝusta versio."}"#);
            send_packet(&mut socket, 0x00, &kick_packet_buf, false).await?;
            return Ok(());
        }
    };

    let host = read_string(&mut pbuf, 255).await?;

    let port = read_ushort(&mut pbuf)?;

    if pbuf.len() != 1 {
        anyhow::bail!("failed to read packet");
    }

    match pbuf[0] {
        1 => {
            let mut out_init_packet_buf = bytes::BytesMut::new();
            write_varint(&mut out_init_packet_buf, version_code);
            write_string(&mut out_init_packet_buf, &host);
            write_ushort(&mut out_init_packet_buf, port);
            write_varint(&mut out_init_packet_buf, 1);

            let mut out_conn = establish_upstream_connection().await?;
            send_packet(&mut out_conn, 0, &out_init_packet_buf, false).await?;

            println!("forwarding");

            // start forwarding
            out_conn.write_all(&buf).await?;

            let (mut socket_reader, mut socket_writer) = tokio::io::split(socket);
            let (mut out_conn_reader, mut out_conn_writer) = tokio::io::split(out_conn);

            futures_util::try_join!(
                tokio::io::copy(&mut socket_reader, &mut out_conn_writer),
                tokio::io::copy(&mut out_conn_reader, &mut socket_writer),
            )?;
        }
        2 => {
            let login_start_packet = read_packet(&mut socket, &mut buf, false).await?;

            if login_start_packet.packet_id != 0 {
                anyhow::bail!("unexpected packet at login start");
            }

            let mut pbuf = login_start_packet.data;

            let req_username = read_string(&mut pbuf, 16).await?;
            if req_username.len() > 15 {
                anyhow::bail!("username is longer than allowed");
            }
            for ch in req_username.chars() {
                if !(ch.is_alphanumeric() || ch == '_') {
                    anyhow::bail!("username contains disallowed characters");
                }
            }

            let username = format!("-{}", req_username);
            let lookup_username = req_username.to_ascii_lowercase();

            let mut hasher = md5::Md5::new();

            hasher.update("OfflinePlayer:".as_bytes());
            hasher.update(&username);

            let mut uuid_bytes = [0; 16];
            uuid_bytes.copy_from_slice(&hasher.finalize()[..16]);

            let uuid = uuid::Builder::from_md5_bytes(uuid_bytes).into_uuid();

            let mut set_compression_packet_buf = bytes::BytesMut::new();
            write_varint(&mut set_compression_packet_buf, 512);

            send_packet(&mut socket, 0x03, &set_compression_packet_buf, false).await?;

            let mut login_success_packet_buf = bytes::BytesMut::new();
            write_uuid(&mut login_success_packet_buf, uuid);
            write_string(&mut login_success_packet_buf, &username);
            write_varint(&mut login_success_packet_buf, 0);

            send_packet(&mut socket, 2, &login_success_packet_buf, true).await?;

            let mut play_login_packet_buf = bytes::BytesMut::new();
            write_int(&mut play_login_packet_buf, 0); // Entity ID
            write_bool(&mut play_login_packet_buf, false); // Is hardcore
            write_ubyte(&mut play_login_packet_buf, 3); // Gamemode
            write_byte(&mut play_login_packet_buf, -1); // Previous Gamemode
            write_varint(&mut play_login_packet_buf, 0); // Dimension Count
            play_login_packet_buf.extend_from_slice(&include_bytes!("./registry_codec.nbt")[..]);
            write_string(&mut play_login_packet_buf, "minecraft:overworld"); // Dimension Type
            write_string(&mut play_login_packet_buf, "minecraft:overworld"); // Dimension Name
            write_long(&mut play_login_packet_buf, 0); // Hashed seed
            write_varint(&mut play_login_packet_buf, 0); // Max Players
            write_varint(&mut play_login_packet_buf, 2); // View Distance
            write_varint(&mut play_login_packet_buf, 0); // Simulation Distance
            write_bool(&mut play_login_packet_buf, false); // Reduced Debug Info
            write_bool(&mut play_login_packet_buf, false); // Enable respawn screen
            write_bool(&mut play_login_packet_buf, false); // Is Debug
            write_bool(&mut play_login_packet_buf, false); // Is Flat
            write_bool(&mut play_login_packet_buf, false); // Has death location

            send_packet(&mut socket, 0x25, &play_login_packet_buf, true).await?;

            let mut spawn_position_packet_buf = bytes::BytesMut::new();
            write_position(&mut spawn_position_packet_buf, 0, 0, 0);
            write_float(&mut spawn_position_packet_buf, 0.0);

            send_packet(&mut socket, 0x4d, &spawn_position_packet_buf, true).await?;

            let mut sync_position_packet_buf = bytes::BytesMut::new();
            write_double(&mut sync_position_packet_buf, 0.0);
            write_double(&mut sync_position_packet_buf, -128.0);
            write_double(&mut sync_position_packet_buf, 0.0);
            write_float(&mut sync_position_packet_buf, 0.0);
            write_float(&mut sync_position_packet_buf, 0.0);
            write_byte(&mut sync_position_packet_buf, 0);
            write_varint(&mut sync_position_packet_buf, 0);
            write_bool(&mut sync_position_packet_buf, false);

            send_packet(&mut socket, 0x39, &sync_position_packet_buf, true).await?;

            let passhash = db.get(&lookup_username)?;
            let mut passhash = passhash
                .as_deref()
                .map(|src| String::from_utf8(src.to_vec()))
                .transpose()?;

            let mut chat_packet_buf = bytes::BytesMut::new();
            write_string(
                &mut chat_packet_buf,
                r#"{
                "text": "Enigu pasvorton por daurigi."
            }"#,
            );
            write_bool(&mut chat_packet_buf, false);

            send_packet(&mut socket, 0x62, &chat_packet_buf, true).await?;

            loop {
                let packet = read_packet(&mut socket, &mut buf, true).await?;

                if packet.packet_id == 0x05 {
                    let mut content = packet.data;
                    let message = read_string(&mut content, 256).await?;

                    let ok = if let Some(hash) = passhash {
                        let (ok, passhash_) = tokio::task::spawn_blocking(|| {
                            let ok = bcrypt::verify(message, &hash);
                            (ok, hash)
                        })
                        .await?;

                        passhash = Some(passhash_);

                        ok?
                    } else {
                        let passhash: String = tokio::task::spawn_blocking(|| {
                            bcrypt::hash(message, bcrypt::DEFAULT_COST)
                        })
                        .await??;

                        db.insert(&lookup_username, passhash.as_bytes())?;

                        true
                    };

                    if ok {
                        let mut out_conn = establish_upstream_connection().await?;

                        let host_for_upstream = format!("{}\0{}\0{}", host, socket_addr.ip(), uuid);

                        let mut out_init_packet_buf = bytes::BytesMut::new();
                        write_varint(&mut out_init_packet_buf, version_code);
                        write_string(&mut out_init_packet_buf, &host_for_upstream);
                        write_ushort(&mut out_init_packet_buf, port);
                        write_varint(&mut out_init_packet_buf, 2);

                        send_packet(&mut out_conn, 0, &out_init_packet_buf, false).await?;

                        let mut out_login_start_packet_buf = bytes::BytesMut::new();
                        write_string(&mut out_login_start_packet_buf, &username);
                        write_bool(&mut out_login_start_packet_buf, false);
                        write_bool(&mut out_login_start_packet_buf, true);
                        write_uuid(&mut out_login_start_packet_buf, uuid);

                        send_packet(&mut out_conn, 0, &out_login_start_packet_buf, false).await?;

                        let mut upstream_buf = bytes::BytesMut::new();

                        let mut got_compression_packet = false;

                        loop {
                            let upstream_packet = read_packet(
                                &mut out_conn,
                                &mut upstream_buf,
                                got_compression_packet,
                            )
                            .await?;
                            println!("got upstream packet {}", upstream_packet.packet_id);
                            if upstream_packet.packet_id == 0x03 {
                                got_compression_packet = true;
                            } else if upstream_packet.packet_id == 0x02 {
                                break;
                            } else {
                                anyhow::bail!(
                                    "unexpected packet from upstream: {} {:?}",
                                    upstream_packet.packet_id,
                                    upstream_packet.data
                                );
                            }
                        }

                        if !got_compression_packet {
                            anyhow::bail!("missing compression upstream");
                        }

                        // wait for next packet (should be 0x25) before forwarding
                        let next_packet_bytes =
                            read_packet_bytes(&mut out_conn, &mut upstream_buf).await?;

                        let mut len_buf = bytes::BytesMut::new();
                        write_varint(&mut len_buf, next_packet_bytes.len() as i32);
                        socket.write_all(&len_buf).await?;
                        socket.write_all(&next_packet_bytes).await?;

                        let mut respawn_packet_buf = bytes::BytesMut::new();
                        write_string(&mut respawn_packet_buf, "minecraft:overworld");
                        write_string(&mut respawn_packet_buf, "minecraft:overworld");
                        write_long(&mut respawn_packet_buf, 0);
                        write_ubyte(&mut respawn_packet_buf, 0);
                        write_byte(&mut respawn_packet_buf, -1);
                        write_bool(&mut respawn_packet_buf, false);
                        write_bool(&mut respawn_packet_buf, false);
                        write_bool(&mut respawn_packet_buf, false);
                        write_bool(&mut respawn_packet_buf, false);

                        send_packet(&mut socket, 0x3e, &respawn_packet_buf, true).await?;

                        // start forwarding
                        out_conn.write_all(&buf).await?;

                        println!("forwarding");
                        socket.write_all(&upstream_buf).await?;

                        let (mut socket_reader, mut socket_writer) = tokio::io::split(socket);
                        let (mut out_conn_reader, mut out_conn_writer) = tokio::io::split(out_conn);

                        futures_util::try_join!(
                            tokio::io::copy(&mut socket_reader, &mut out_conn_writer),
                            tokio::io::copy(&mut out_conn_reader, &mut socket_writer),
                        )?;

                        break;
                    } else {
                        let mut chat_packet_buf = bytes::BytesMut::new();
                        write_string(
                            &mut chat_packet_buf,
                            r#"{
                            "text": "Malĝusta pasvorto."
                        }"#,
                        );
                        write_bool(&mut chat_packet_buf, false);

                        send_packet(&mut socket, 0x62, &chat_packet_buf, true).await?;
                    }
                }
            }
        }
        _ => anyhow::bail!("unknown next state"),
    }

    Ok(())
}

#[tokio::main]
async fn main() {
    let db = sled::open("db").unwrap();

    let listener = tokio::net::TcpListener::bind("0.0.0.0:25566")
        .await
        .unwrap();

    loop {
        match listener.accept().await {
            Ok((socket, socket_addr)) => {
                let db = db.clone();
                tokio::spawn(async move {
                    println!("handling connection");
                    if let Err(err) = handle_connection(socket, socket_addr, db).await {
                        eprintln!("Failed while handling connection: {:?}", err);
                    }
                });
            }
            Err(err) => eprintln!("Failed to accept client: {:?}", err),
        }
    }
}
